# MapTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.1.

## Common setup

Clone the repo and install the dependencies:

```bash
git clone https://gitlab.com/Andreas911/map-test
cd map-test
```

```bash
npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

