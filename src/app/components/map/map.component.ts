import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Map, MapLayerMouseEvent, SymbolLayout } from 'mapbox-gl';
import { Subscription } from 'rxjs';
import { LocationsParams } from 'src/app/models/Location';
import { LanguagesService } from 'src/app/services/languages.service';
import { LocationsService } from 'src/app/services/locations.service';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
	map: Map | undefined;
	locations: GeoJSON.FeatureCollection = {
		type: "FeatureCollection",
		features: []
	};
	config: LocationsParams = {
		pagination: false,
		size: 25,
		page: 1,
		sort: "name",
		order: 1,
		search: ""
	}
	selectedPoint: any;
	layout: SymbolLayout = {
		"text-field": '{point_count_abbreviated}',
		"text-font": ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
		"text-size": 12
	};
	sidenavOpened: boolean = false;
	
	lang: string = "en";
	langSub: Subscription | undefined;

	theme: string = "light";
	themeSub: Subscription | undefined;

	constructor(
		private locationsSrv: LocationsService, 
		private langSrv: LanguagesService, 
		private themeSrv: ThemeService,
		private _snackBar: MatSnackBar
	) { }

	ngOnInit(): void {
		// Set map language based on global language
		this.lang = this.langSrv.getLang();

		// Set map theme based on global theme
		this.theme = this.themeSrv.getTheme();

		// Change map theme on theme change
		this.themeSub = this.themeSrv.themeChange.subscribe(theme => {
			this.theme = theme;
		});
	}

	ngOnDestroy(): void {
		if (this.langSub) this.langSub.unsubscribe();
		if (this.themeSub) this.themeSub.unsubscribe();
	}

	isBiggerScreen(): boolean {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		return width < 768 ? true : false;
	}

	onMapLoaded() {
		// Set map language
		this.map.setLayoutProperty('country-label-lg', 'text-field', `{name_${this.lang}}`);
		// this.map.setLayoutProperty('state-label-lg', 'text-field', `{name_${this.lang}}`);

		// Load data
		if (this.locationsSrv.getTotalLocations() === 0) {
			this.locationsSrv.loadLocations(this.lang).subscribe({
				next: (data: GeoJSON.FeatureCollection) => {
					this.locationsSrv.setLocations(data.features)
					this.onLoadData();
				}, 
				error: (err) => {
					this._snackBar.open("Error loading locations", "OK", {
						panelClass: "error"
					});
				}
			});
		} else this.onLoadData();

		// Reload data & change map language on language change
		this.langSub = this.langSrv.langChange.subscribe(lang => {
			this.onClose();
			this.lang = lang;
			this.map.setLayoutProperty('country-label-lg', 'text-field', `{name_${this.lang}}`);
			// this.map.setLayoutProperty('state-label-lg', 'text-field', `{name_${this.lang}}`);
			this.onLoadData();
		});
	}

	onResize() {
		// this.map?.resize();
	}

	onLoadData() {
		const features: GeoJSON.Feature[] = this.locationsSrv.getLocations(this.config).docs;
		this.locations = {
			type: "FeatureCollection",
			features: features
		}
	}

	onClick(e: MapLayerMouseEvent) {
		this.selectedPoint = e.features?.[0];
		this.sidenavOpened = true;
	}

	onClose() {
		this.selectedPoint = false;
		this.sidenavOpened = false;
	}
}
