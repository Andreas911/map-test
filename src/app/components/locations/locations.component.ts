import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { LocationsApi, LocationsParams } from 'src/app/models/Location';
import { LanguagesService } from 'src/app/services/languages.service';
import { LocationsService } from 'src/app/services/locations.service';

@Component({
	selector: 'locations',
	templateUrl: './locations.component.html',
	styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit, OnDestroy {
	data: LocationsApi = {
		docs: [],
		hasNextPage: false,
		hasPrevPage: false,
		limit: 25,
		nextPage: 0,
		page: 1,
		pagingCounter: 0,
		prevPage: 0,
		totalDocs: 0,
		totalPages: 1
	};

	config: LocationsParams = {
		pagination: true,
		size: 10,
		page: 1,
		sort: "name",
		order: 1,
		search: ""
	}

	showSearch: boolean = false;
	selected: GeoJSON.Feature = null;
	langSub: Subscription | undefined;

	constructor(
		private locationsSrv: LocationsService,
		private languagesSrv: LanguagesService,
		public dialog: MatDialog,
		private _snackBar: MatSnackBar
	) { }

	ngOnInit(): void {
		// Get global language
		const lang = this.languagesSrv.getLang();

		// Load data
		if (this.locationsSrv.getTotalLocations() === 0) {
			this.locationsSrv.loadLocations(lang).subscribe({
				next: (data: GeoJSON.FeatureCollection) => {
					this.locationsSrv.setLocations(data.features)
					this.onLoadData();
				},
				error: (err) => {
					this._snackBar.open("Error loading locations", "OK", {
						panelClass: "error"
					});
				}
			});
		} else this.onLoadData();

		// Reload data on language change
		this.langSub = this.languagesSrv.langChange.subscribe(lang => {
			this.onLoadData();
		});
	}

	ngOnDestroy(): void {
		if (this.langSub) this.langSub.unsubscribe(); 
	}

	onLoadData(): void {
		this.data = this.locationsSrv.getLocations(this.config);
	}

	onSearch(query: string) {
		this.config.search = query.toLowerCase();
		this.onFirstPage();
	}

	onSortBy(sort: string, order: 1 | -1) {
		this.config.sort = sort;
		this.config.order = order;
		this.onLoadData();
	}

	onFirstPage() {
		this.config.page = 1;
		this.onLoadData();
	}

	onPrevPage() {
		if (this.data.hasPrevPage) {
			this.config.page = this.config.page - 1;
			this.onLoadData();
		}
	}

	onNextPage() {
		if (this.data.hasNextPage) {
			this.config.page = this.config.page + 1;
			this.onLoadData();
		}
	}

	onLastPage() {
		if (this.data.page < this.data.totalPages) {
			this.config.page = this.data.totalPages;
			this.onLoadData();
		}
	}

	onSetPageSize(size: number) {
		this.config.page = 1;
		this.config.size = size;
		this.onLoadData();
	}

	onAdd(): void {
		const dialogRef = this.dialog.open(LocationDialog, {
			width: "500px",
			maxWidth: "calc(100% - 20px)",
			data: {
				title: "New Location",
				location: {
					type: "Feature",
					properties: {
						name: ""
					},
					geometry: {
						type: "Point",
						coordinates: [null, null]
					}
				}
			},
		});
	  
		dialogRef.afterClosed().subscribe(location => {
			if (!location) return;
			this.locationsSrv.addLocation(location);
			this.onLoadData();
			this._snackBar.open("Location added", "OK", {
				panelClass: "success",
				duration: 2000
			});
		});
	}

	onEdit() {
		const dialogRef = this.dialog.open(LocationDialog, {
			width: "500px",
			maxWidth: "calc(100% - 20px)",
			data: {
				title: "Edit Location",
				location: this.selected
			},
		});
	  
		dialogRef.afterClosed().subscribe(location => {
			if (!location) return;
			this.locationsSrv.editLocation(this.selected.properties['id'], location);
			this.onLoadData();
			this._snackBar.open("Location added", "OK", {
				panelClass: "success",
				duration: 2000
			});
		});
	}

	onDelete() {
		const dialogRef = this.dialog.open(DeleteDialog, {
			width: "500px",
			maxWidth: "calc(100% - 20px)"
		});

		dialogRef.afterClosed().subscribe(confirm => {
			console.log(confirm);
			if (!confirm) return;
			this.locationsSrv.deleteLocation(this.selected.properties['id']);
			if (this.data.docs.length === 1) {
				if (this.data.hasPrevPage) this.onPrevPage();
				else this.onFirstPage();
			} else {
				this.onLoadData();
			}
			this._snackBar.open("Location deleted", "OK", {
				panelClass: "success",
				duration: 2000
			});
		});
	}
}

@Component({
	selector: 'location-dialog',
	templateUrl: 'location-dialog.html',
})
export class LocationDialog {
	form: FormGroup;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		public dialogRef: MatDialogRef<LocationDialog>
	) {
		this.form = new FormGroup({
			name: new FormControl(data.location.properties.name, [
				Validators.required,
				Validators.maxLength(64)
			]),
			lng: new FormControl(data.location.geometry.coordinates[0], [
				Validators.required
			]), 
			lat: new FormControl(data.location.geometry.coordinates[1], [
				Validators.required
			])
		});
	}

	onSubmit() {
		if (this.form.invalid) return;

		const payload: GeoJSON.Feature = {
			type: "Feature",
			properties: {
				name: this.form.value.name
			},
			geometry: {
				type: "Point",
				coordinates: [this.form.value.lng, this.form.value.lat]
			}
		};

		this.dialogRef.close(payload);
	}
}

@Component({
	selector: 'delete-dialog',
	templateUrl: 'delete-dialog.html',
})
export class DeleteDialog {

	constructor() {}

}