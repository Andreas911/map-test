import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocationsApi, LocationsParams } from '../models/Location';

@Injectable({
	providedIn: 'root'
})
export class LocationsService {
	files: Record<string, string> = {
		en: "assets/locations.json",
		fr: "assets/locations_fr.json"
	}
	docs: GeoJSON.Feature[] = [];

	constructor(
		private httpClient: HttpClient
	) { }

	loadLocations(lang?: string): Observable<GeoJSON.FeatureCollection> {
		return this.httpClient.get<GeoJSON.FeatureCollection>(this.files[lang ? lang : "en"]);
	}

	setLocations(locations: GeoJSON.Feature[]) {
		this.docs = locations;
	}

	getLocations(config: LocationsParams): LocationsApi {
		let res: GeoJSON.Feature[] = JSON.parse(JSON.stringify(this.docs));

		// Text Search
		if (config.search.length > 0) {
			res = res.filter((el: GeoJSON.Feature)=>{
				if (
					el.properties['name'].toLowerCase().includes(config.search) ||
					el.properties['id'].includes(config.search)
				) return true;
				else return false;
			});
		}

		// Filter

		// Sort
		res.sort(
			(a: GeoJSON.Feature, b: GeoJSON.Feature) => (a.properties[config.sort] > b.properties[config.sort]) ? 1 : -1
		);
		if (config.order === -1) res.reverse();

		const totalDocs: number = res.length;
		const limit: number = config.size;
		const pagingCounter: number = (config.page - 1) * config.size + 1;
		const totalPages: number = config.pagination ? Math.ceil(res.length / config.size) : 1;
		const hasPrevPage: boolean = config.page > 1;
		const hasNextPage: boolean = config.page < totalPages;
		const page: number = config.page;

		// Paginate
		if (config.pagination) {
			res = res.slice(pagingCounter - 1, pagingCounter - 1 + config.size);
		}

		return {
			docs: res,
			totalDocs: totalDocs,
			limit: limit,
			hasPrevPage: hasPrevPage,
			hasNextPage: hasNextPage,
			page: page,
			totalPages: totalPages,
			prevPage: null,
			nextPage: null,
			pagingCounter: pagingCounter
		};
	}

	getTotalLocations():number {
		return this.docs.length;
	}

	addLocation(location: GeoJSON.Feature) {
		location.properties['id'] = new Date().getTime().toString();
		this.docs.push(location);
	}

	editLocation(id: string, location: GeoJSON.Feature) {
		for (let i=0; i<this.docs.length; i++) {
			if (this.docs[i].properties['id'] === id) {
				this.docs[i].properties['name'] = location.properties['name'];
				this.docs[i].geometry['coordinates'] = location.geometry['coordinates'];
				return;
			}
		}
	}

	deleteLocation(id: string) {
		for (let i=0; i<this.docs.length; i++) {
			if (this.docs[i].properties['id'] === id) {
				this.docs.splice(i, 1);
			}
		}
	}
}
