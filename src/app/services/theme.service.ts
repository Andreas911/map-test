import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ThemeService {
	private themes: Record<string, string> = {
		"light": "Light Theme",
		"dark": "Dark Theme"
	};
	private defaultTheme: string = "light";
	private themeChangeSrc = new BehaviorSubject<string>(this.defaultTheme);
	themeChange = this.themeChangeSrc.asObservable();


	constructor() {
		this.setTheme(this.getTheme());
	}

	setTheme(theme: string): string {
		if (!this.themes[theme]) theme = this.defaultTheme;
		localStorage.setItem("theme", theme);
		document.documentElement.setAttribute("data-theme", theme);
		this.themeChangeSrc.next(theme);
		return theme;
	}

	getTheme(): string {
		const theme = localStorage.getItem("theme");
		if (theme !== null && this.themes[theme]) return theme;
		else return this.defaultTheme;
	}
}
