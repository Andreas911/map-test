import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class LanguagesService {
	private languages: string[] = ["en", "fr"];
	private defaultLang: string = "en";
	private langChangeSrc = new BehaviorSubject<string>(this.defaultLang);
	langChange = this.langChangeSrc.asObservable();

	constructor() {
		this.setLang(this.getLang());
	}

	setLang(lang: string): string {
		if (!this.languages.includes(lang)) lang = this.defaultLang;
		localStorage.setItem("lang", lang);
		this.langChangeSrc.next(lang);
		return lang;
	}

	getLang(): string {
		const lang = localStorage.getItem("lang");
		if (lang !== null && this.languages.includes(lang)) return lang;
		else return this.defaultLang;
	}
}
