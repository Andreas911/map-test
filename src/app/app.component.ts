import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, Subscription, switchMap } from 'rxjs';
import { LanguagesService } from './services/languages.service';
import { LocationsService } from './services/locations.service';
import { ThemeService } from './services/theme.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
	private titlePrefix: string = "TRG"
	title: string = "";
	theme: string = "";
	lang: string = "";
	loading: boolean = false;
	locationsSub: Subscription | undefined;

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private titleDoc: Title,
		private locationsSrv: LocationsService,
		private languagesSrv: LanguagesService,
		private themeSrv: ThemeService,
		private _snackBar: MatSnackBar
	) {}

	ngOnInit(): void {
		// Set Language
		this.lang = this.languagesSrv.getLang();

		// Set Theme
		this.theme = this.themeSrv.getTheme();

		// Update title on route change
		this.router.events.pipe(
			filter(event => event instanceof NavigationEnd),
			map(() => this.activatedRoute),
			map((route) => {
				while (route.firstChild) route = route.firstChild;
				return route;
			}),
			switchMap(route => route.data),
			map((data) => {
				return data['title'] ? data['title'] : "";
			})
		).subscribe((pathStr: string) => {
			this.title = pathStr;
			this.titleDoc.setTitle(`${this.titlePrefix} - ${pathStr}`);
		});
	}

	ngOnDestroy(): void {
		if (this.locationsSub) this.locationsSub.unsubscribe();
	}

	onActivate(e: any) {
		window.scroll(0, 0);
	}

	onSetLanguage(lang: string) {
		if (this.lang === lang) return;

		this.loading = true;
		this.locationsSub = this.locationsSrv.loadLocations(lang).subscribe({
			next: (data: GeoJSON.FeatureCollection) => {
				this.locationsSrv.setLocations(data.features)
				this.lang = this.languagesSrv.setLang(lang);
				this.loading = false;
			}, 
			error: (err) => {
				console.log(err);
				this.loading = false;
				this._snackBar.open("Error loading locations", "OK", {
					panelClass: "error"
				});
			}
		});
	}

	onToggleTheme() {
		if (this.theme === "light") this.theme = this.themeSrv.setTheme("dark");
		else this.theme = this.themeSrv.setTheme("light");
	}
}
