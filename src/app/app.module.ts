import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from './material-module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';
import { ErrorComponent } from './components/error/error.component';
import { LocationsComponent, LocationDialog, DeleteDialog } from './components/locations/locations.component';

import { LocationsService } from './services/locations.service';
import { ThemeService } from './services/theme.service';
import { LanguagesService } from './services/languages.service';

@NgModule({
	declarations: [
		AppComponent,
		ErrorComponent,
		MapComponent,
		LocationsComponent,
		LocationDialog,
		DeleteDialog
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		AngularMaterialModule,
		NgxMapboxGLModule.withConfig({
			accessToken: 'pk.eyJ1IjoiYWFuZHJlMjgiLCJhIjoiY2w2aDMxbXoyMGs4YjNibzZsbWlieXk5eiJ9.j-RnNiP5szXjn4QcVPfMog',
		})
	],
	providers: [
		LocationsService, 
		LanguagesService, 
		ThemeService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
