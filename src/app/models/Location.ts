export interface LocationsParams {
	pagination: boolean;
	size: number;
	page: number;
	sort: string;
	order: 1 | -1;
	search: string;
}

export interface LocationsApi {
	docs: GeoJSON.Feature[];	// Array of documents
	totalDocs: number;			// Total number of documents in collection that match a query
	limit: number;				// Limit that was used
	hasPrevPage: boolean;		// Availability of prev page
	hasNextPage: boolean;		// Availability of next page.
	page: number;				// Current page number
	totalPages: number;			// Total number of pages
	prevPage: number | null;	// Previous page number if available or NULL
	nextPage: number | null;	// Next page number if available or NULL
	pagingCounter: number;		// The starting index/serial/chronological number of first document in current page. (Eg: if page=2 and limit=10, then pagingCounter will be 11)
}