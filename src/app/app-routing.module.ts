import { NgModule } from '@angular/core';
import { ExtraOptions, PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { MapComponent } from './components/map/map.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ErrorComponent } from './components/error/error.component';

const routes: Routes = [{
	path: '',
	redirectTo: '/map',
	pathMatch: 'full'
}, {
	path: 'map',
	component: MapComponent,
	data: { title: 'Map Page' }
}, {
	path: 'locations',
	component: LocationsComponent,
	data: { title: 'Locations Page' }
}, {
	path: '**',
	component: ErrorComponent,
	data: { title: 'Error' }
}];

const routerOptions: ExtraOptions = {
    preloadingStrategy: PreloadAllModules
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
